// === BASE SETUP ===========================================

var express = require('express'); //express
var app     = express(); // app
var port    =   process.env.PORT || 8080; // port
var api = require('./routes/api'); // api
var bodyParser = require('body-parser'); // body parser

// === MIDDLEWARE ===========================================

app.use(bodyParser.urlencoded({ extended: false })); // flag

app.all('/*', function(req, res, next) { // set headers
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/api', api); // use api

// === ROUTES ===========================================

// we'll create our routes here
app.get('/', function(req, res) {
    res.sendStatus(200);  
});

// === START THE SERVER ===========================================
app.listen(port); // start
console.log('Listening to application on port ' + port);
