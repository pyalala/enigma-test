var express = require('express'); // express
var datam = require('./datam'); // datam middleware
var router = express.Router(); // router
var bodyParser = require('body-parser'); // bodyparser

// route middleware that will happen on every request
router.use(function(req, res, next) {
	// log each request to the console
	console.log(req.method, req.url);
	next();
});
// create application/json parser 
var jsonParser = bodyParser.json();
 
// create application/x-www-form-urlencoded parser 
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// api router
router.route('/echo') // when user requests '/echo'
	.post(function(req,res){
		// stop request if there are no parameters
		if (!req.body) return res.sendStatus(400)
		var msg = req.body.mesg, // message to send
		datam_proxy = datam.echo(msg); // pass message to middleware
		res.json({message: datam_proxy}); // respond back with output
	});

router.route('/postItems') // when user requests '/postItems'
	.post(function(req, res){
		// stop request if there are no parameters
		if (!req.body) return res.sendStatus(400)
		var cat = req.body.category; // get category from request
		var desc = req.body.description;  // get description from request
		var amt = req.body.amount;  // get amount from request
		// check if any of parameters are null
		if(typeof(cat) === "undefined" || typeof(desc) === "undefined" || typeof(amt) === "undefined"){
			return res.sendStatus(422); // terminate request with "bad request error"
		}
		datam.addItem(cat, desc, amt); // add items to middleware cache
		console.log("added record", cat, desc, amt); // console log request
		res.sendStatus(200); // finish response
	});

router.route('/getItemsLength') // when user requests '/getItemsLength'
	.get(function(req, res){
		var dataSize = datam.getItemSize(); // fetch item size from middleware
		res.json({size: dataSize}); // respond back sending size object
		console.log(dataSize); // console log request
		res.end(); // end request
	});

router.route('/getCategories') // when user requests '/getCategories'
	.get(function(req, res){
		var categories = datam.getCategories(); // fetch items from middleware
		res.json({categories: categories}); // respond back sending categories object
		console.log(categories); // console log request
		res.end(); // end request
	});

router.route('/getItems') // when user requests '/getItems'
	.get(function(req, res){
		var data = datam.getItems(); // fetch items from middleware
		if(data !== null){ // if there are items
			res.json(data); // send items
		}else{ // no items found
			res.json({error: null}); // send object with null error
		}
		res.end(); // end request
	});

module.exports = router;
