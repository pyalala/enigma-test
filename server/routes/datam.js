var datam = function() {
	var items = {}; // items cache
	/**
	 * categories are hard coded to show that the application
	 * can GET something from the server
	 */
	var categories = ['Entertainment', 'Travel', 'Shopping', 'Food', 'Savings', 'Donation', 'Bills', 'Other'];
	/**
	 * adds items to cache
	 * @param {string} cat
	 * @param {string} desc
	 * @param {string} amt
	 */
	var addItemFn = function(cat, desc, amt){
		var itemCount = 0; // item count

		if(typeof(items) !== "undefined"){ // if there are items
			itemCount = Object.keys(items).length; // get item length
			itemCount++; // increment
		}
		// add items to cache
		items[itemCount] = {
			category: cat,
			description: desc,
			amount: amt,
		};
	};
	/**
	 * function returns all categories
	 * @return {object} categories
	 */
	var getCategoriesFn = function(){
		return categories;
	};
	/**
	 * function returns item size
	 * @return {int} Object.keys(items).length
	 */
	var getItemSizeFn = function(){
		return Object.keys(items).length;
	};
	/**
	 * function returns items
	 * @return {object} items
	 */
	var getItemsFn = function(){
		var itemCount = Object.keys(items).length;
		if(itemCount === 0){
			return null;
		}else{
			return items;
		}
	};
	/**
	 * function returns string passed to it
	 * @return {String} str
	 */
	var echoFn = function(str){
		return str;
	};
	// return object with public accessible functions
	return {
		// function adds items to cache
		addItem: function(cat, desc, amt){
			addItemFn(cat, desc, amt);
		},
		// function returns item cache size
		getItemSize: function(){
			return getItemSizeFn();
		},
		// function returns all items
		getItems: function(){
			return getItemsFn();
		},
		// function returns any string passed
		echo : function(str){
			return echoFn(str);
		},
		// function returns categories
		getCategories : function(){
			return getCategoriesFn();
		}
	};
}

module.exports = datam();
