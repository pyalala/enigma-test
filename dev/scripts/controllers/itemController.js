
// === Controllers ========================================

/**
 * This module's itemController primarily manages addItem page
 * and its components.
 */
angular.module("enigma.items",[]).controller("itemController", function ($scope, $location, xhrService, apiConfig){
	// categories
	$scope.categories= {};
	// form UI components
	$scope.formData= {
		category: "",
		amount : "",
		description: ""
	};
	// pristine component data
	var staticFormData = angular.copy($scope.formData);
	/**
	 * redirects user to location "/" 
	 */
	$scope.goHome = function () {
		$location.path("/"); // navigare user
	};
	/**
	 * resets the form to default settings 
	 */
	$scope.resetForm = function(){
		$scope.formData = angular.copy(staticFormData); //  reset form values
		$scope.newItemForm.$setPristine(); // set form to pristine
	};
	/**
	 * fetches categories from API
	 */
	$scope.getCategories = function() {
		// getCategories promise object
		var promise = xhrService.getData(apiConfig.backend+'/getCategories');
		promise.then(
			function(payload) { // success
				$scope.categories = payload.data.categories; // save data
			},
			function(errorPayload) { // failure
				alert("Failed to fetch data!");
				console.error('Failed to fetch data!', errorPayload);
			}
		);
	};
	/**
	 * saves form data to API
	 * @param {Object} payloadData
	 */
	$scope.saveData = function(payloadData){
		// Post Item object/promise
		var datainfo = xhrService.postData(apiConfig.backend+'/postItems', payloadData);
		datainfo.then(function(respData){
			if(respData.status === 200){ // posted successfully
				console.info("Posted Successfully!");
				alert("Posted Successfully!");
				$scope.resetForm(); // reset form
			}
		}).catch(function(respData){ // post failure
			if(respData.status !== 200 || respData.status === 0){
				console.error("Something went wrong!", respData);
				alert("Something went wrong!");
			}
		});
	};
	/**
	 * function to initiate actual API call
	 * @param {boolean} isValid
	 */
	$scope.addItem = function(isValid){
		if (isValid) { // if form is valid
			$scope.saveData($scope.formData);
		}
	};
	// fetch categories
	$scope.getCategories();
});

