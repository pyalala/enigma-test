
// === Controllers ========================================

/**
 * This module's mainController manages to retrieve data from
 * server and generates UI.
 */

angular.module("enigma.main",[]).controller("mainController", function ($scope, $location, xhrService, apiConfig){
	$scope.appTitle = apiConfig.appTitle; // application title
	$scope.currentYear = new Date().getFullYear(); // current year
	/**
	 * redirects user to location "/add"
	 */
	$scope.goAdd = function(){
		$location.path("/add");
	};
	// items
	$scope.items = {};
	// categories
	$scope.categories= [];
	/**
	 * fetches total number of items from backend
	 */
	$scope.getItemsCount = function() {
		// getItemsLength promise object
		var promise = xhrService.getData(apiConfig.backend+'/getItemsLength');
		promise.then(
			function(payload) { //success 
				$scope.itemCount = payload.data.size; // items
				if($scope.itemCount>0){ // has items
					$scope.getCategories(); // save items
				}
			},
			function(errorPayload) { // failure
				alert("Failed to fetch data!");
				console.error('Failed to fetch data!', errorPayload);
			}
		);
	};
	/**
	 * fetches categories from backend
	 */
	$scope.getCategories = function() {
		// getCategories promise object
		var promise = xhrService.getData(apiConfig.backend+'/getCategories');
		promise.then(
			function(payload) { // success
				$scope.categories = payload.data.categories; // categories
				$scope.getData(); // begin fetching data
			},
			function(errorPayload) { // failure
				alert("Failed to fetch data!");
				console.error('Failed to fetch data!', errorPayload);
			}
		);
	};
	/**
	 * fetches items from backend
	 */
	$scope.getData= function(){
		// getItems promise object
		var promise = xhrService.getData(apiConfig.backend+'/getItems');
		promise.then(
			function(payload) {  // success
				var items = payload.data; // items
				var amount = [], category = []; // arrays to split data for UI
				for(var x in items){
					amount.push(items[x].amount);
					category.push(items[x].category);
				}
				$scope.data = amount; // amount data
				$scope.labels = category; // category data
			},
			function(errorPayload) { // failure
				alert("Failed to fetch data!");
				console.error('Failed to fetch data!', errorPayload);
			}
		);
	};
	// fetch categories
	$scope.getItemsCount();
});

