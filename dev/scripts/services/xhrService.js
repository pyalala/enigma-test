
// === Services/Factories ========================================

/**
 * This module's factory service returns an service object which can be used
 * to fetch (GET) or send (POST) data to server
 */

angular.module("enigma.xhr", []).factory('xhrService', function($http) {
	return {
		/**
		 * sends data to server
		 * @param {string} url 
		 * @param {object} data 
		 */
		postData: function(url, data){
			return $http.post(
				url, // backend api url
				$.param(data), // format data
				{
					headers : { // headers to send
						'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
					}
    				}
			);
		},
		/**
		 * gets data from server
		 * @param {string} url 
		 * @returns {object} $http 
		 */
		getData: function(url) {
			return $http.get(url);
		}
	}
});


