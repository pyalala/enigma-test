
// === Config  ========================================

/**
 * ChartJS Angular provider configuration
 */

angular.module("enigma.charts",['chart.js']).config(function (ChartJsProvider) {
	// Configure options for doughnut chart
	ChartJsProvider.setOptions('Doughnut', {
		animateScale: true,
		colours: ['#97BBCD', '#DCDCDC', '#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
		responsive: true,
		maintainAspectRatio: true,
		animationEasing: "easeOutQuart",
		animation: true,
		animationSteps: 60
	});
});

