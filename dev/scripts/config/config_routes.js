
// === Config  ========================================

/**
 * Application route configuration
 */

angular.module("enigma.routes", ['ngRoute']).config(
	function($routeProvider) {
	        $routeProvider
                .when('/',{  // when user visits '/'
			templateUrl: 'dist/templates/main.html',
			controller: 'mainController'
		})
		.when('/add', {  // when user visits '/add'
			templateUrl: 'dist/templates/addItems.html',
			controller: 'itemController'
		});
	}
);

