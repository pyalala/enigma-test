
// === Config  ========================================

/**
 * General configuration
 */

angular.module("enigma.config",[]).constant('apiConfig', {
  'backend': 'http://localhost:3000/api', // backend api url
  'appTitle' : 'Enigma Demo' // application title
});
