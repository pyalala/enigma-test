var controllers = ['enigma.main','enigma.items'];
var configs = ['enigma.config', 'enigma.routes','enigma.charts'];
var services = ['enigma.xhr'];
var injector = controllers.concat(configs.concat(services));

var enigma = angular.module("enigmaApp", injector);


