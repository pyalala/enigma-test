var enigma = angular.module("enigmaApp", ['ngRoute']);

// === Config  ========================================

enigma.config(function($routeProvider) {
        $routeProvider
                .when('/',{
			templateUrl: 'dist/templates/main.html',
			controller: 'mainController'
		})
		.when('/add', {
			templateUrl: 'dist/templates/addItems.html',
			controller: 'itemController'
		});
});

// === Controllers ========================================

enigma.controller("mainController", function ($scope, $location){
	$scope.appTitle = "Enigma Demo";
	$scope.currentYear = new Date().getFullYear();
	$scope.endPoint = "http://localhost:3000/api";
	$scope.goAdd = function(){
		console.log("hello");
		$location.path("/add");
	}	
});

enigma.controller("itemController", function ($scope, $location, xhrService){
	$scope.endPoint = "http://localhost:3000/api";
	$scope.categories= {};
	$scope.goHome = function () {
		console.log("hello");
		$location.path("/");
	};
	$scope.getCategories = function() {
		var promise = xhrService.getData($scope.endPoint+'/getCategories');
		promise.then(
		function(payload) { 
			$scope.categories = payload.data;
		},
		function(errorPayload) {
			$log.error('failed to fetch data', errorPayload);
		});
	};
	$scope.getCategories();
});

enigma.factory('xhrService', function($http) {
	return {
		getData: function(url) {
			return $http.get(url);
		}
	}
});

