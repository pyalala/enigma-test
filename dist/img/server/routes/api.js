var express = require('express');
var datam = require('./datam');
var router = express.Router();
var bodyParser = require('body-parser');

// route middleware that will happen on every request
router.use(function(req, res, next) {
	// log each request to the console
	console.log(req.method, req.url);
	next();
});
// create application/json parser 
var jsonParser = bodyParser.json();
 
// create application/x-www-form-urlencoded parser 
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// api
router.route('/echo')
	.post(function(req,res){
		if (!req.body) return res.sendStatus(400)
		var msg = req.body.mesg;
		console.log(datam.echo(msg));
		res.end();
	});

router.route('/postItems')
	.post(function(req, res){
		if (!req.body) return res.sendStatus(400)
		var cat = req.body.category;
		var desc = req.body.description;
		var amt = req.body.amount;
		datam.addItem(cat, desc, amt);
		res.end();
	});

router.route('/getItemsLength')
	.get(function(req, res){
		var dataSize = datam.getItemSize();
		res.json({size: dataSize});
		console.log(dataSize);
		res.end();
	});

router.route('/getCategories')
	.get(function(req, res){
		var categories = datam.getCategories();
		res.json({categories: categories});
		console.log(categories);
		res.end();
	});

router.route('/getItems')
	.get(function(req, res){
		var data = datam.getItems();
		if(data !== null){
			res.json(data);
		}else{
			res.json({error: "null data"});		
		}
		res.end();
	});

module.exports = router;
