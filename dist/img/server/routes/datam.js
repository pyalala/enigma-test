var datam = function() {
	var items = {};
	var categories = ['Entertainment', 'Travel', 'Shopping', 'Food', 'Savings', 'Donation', 'Bills'];
	var addItemFn = function(cat, desc, amt){
		var itemCount = 0;

		if(typeof(items) !== "undefined"){
			itemCount = Object.keys(items).length;
			itemCount++;
		}
		items[itemCount] = {
			category: cat,
			description: desc,
			amount: amt,
		};
	};
	var getCategoriesFn = function(){
		return categories;
	};
	var getItemSizeFn = function(){
		return Object.keys(items).length;
	};
	var getItemsFn = function(){
		var itemCount = Object.keys(items).length;
		if(itemCount === 0){
			return null;
		}else{
			return items;
		}
	};
	var echoFn = function(str){
		return str;
	};

	return {
		addItem: function(cat, desc, amt){
			addItemFn(cat, desc, amt);
		},
		getItemSize: function(){
			return getItemSizeFn();
		},
		getItems: function(){
			return getItemsFn();
		},
		echo : function(str){
			return echoFn(str);
		},
		getCategories : function(){
			return getCategoriesFn();
		}
	};
}

module.exports = datam();
