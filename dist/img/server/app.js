// server.js

// BASE SETUP
// ==============================================

var express = require('express');
var app     = express();
var port    =   process.env.PORT || 8080;
var api = require('./routes/api');
var bodyParser = require('body-parser');

// MIDDLEWARE
// ==============================================

app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api);

// ROUTES
// ==============================================

// we'll create our routes here
app.get('/', function(req, res) {
    res.sendStatus(200);  
});

// START THE SERVER
// ==============================================
app.listen(port);
console.log('Listening to application on port ' + port);
