'use strict';

module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		paths: {
			angular: {
				lib: "bower_components/angular",
				route: "bower_components/angular-route",
				charts: "bower_components/angular-chart.js"
			},
			bootstrap: {
				fonts: "bower_components/bootstrap-sass/assets/fonts/bootstrap",
				js: "bower_components/bootstrap-sass/assets/javascripts"
			},
			fontAwesome: "bower_components/fontawesome/fonts",
			jquery: "bower_components/jquery/dist",
			chart: "bower_components/Chart.js",
			dev: {
				styles: "dev/styles",
				scripts: "dev/scripts",
				images: "dev/img",
				fonts: "dev/fonts/",
				templates: "dev/templates",
			},
			dist: {
				js: "dist/js",
				fonts: "dist/fonts/",
				css: "dist/css",
				img: "dist/img",
				templates: "dist/templates",
			},
			express: "server"
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				eqnull: true,
				browser: true,
				globals: {
					jQuery: true
				},
			},
			all: ["<%=paths.dev.js%>/**/*.js"]
		},
		uglify: {
			options: {
				mangle: false,
				compress: false,
				beautify: true
			},
			scripts: {
				files: {
					"<%=paths.dist.js%>/<%=pkg.name%>.js" :[
						"<%=paths.chart%>/Chart.js",
						"<%=paths.angular.lib%>/angular.js",
						"<%=paths.angular.route%>/angular-route.js",
						"<%=paths.angular.charts%>/angular-chart.js",
						"<%=paths.jquery%>/jquery.js",
						"<%=paths.bootstrap.js%>/bootstrap.js",
						"<%=paths.dev.scripts%>/config/config_enigma.js",
						"<%=paths.dev.scripts%>/config/config_chartjs.js",
						"<%=paths.dev.scripts%>/config/config_routes.js",
						"<%=paths.dev.scripts%>/services/xhrService.js",
						"<%=paths.dev.scripts%>/controllers/mainController.js",
						"<%=paths.dev.scripts%>/controllers/itemController.js",
						"<%=paths.dev.scripts%>/application.js",
					]
				}
			}
		},
		copy: {
			fonts: {
				files: [
					{
					    expand: true,
					    cwd: "<%=paths.bootstrap.fonts%>",
					    src: ['**'],
					    dest: "<%=paths.dist.fonts%>/bootstrap"
					},
					{
					    expand: true,
					    cwd: "<%=paths.dev.fonts%>",
					    src: ['**'],
					    dest: "<%=paths.dist.fonts%>"
					},
					{
					    expand: true,
					    cwd: "<%=paths.fontAwesome%>",
					    src: ['**'],
					    dest: "<%=paths.dist.fonts%>/fontawesome"
					}
				]
			},
			images: {
				files:[
					{
					    expand: true,
					    cwd: "<%=paths.dev.images%>",
					    src: ['**'],
					    dest: "<%=paths.dist.img%>"
					}
				]
			},
			templates: {
				files:[
					{
					    expand: true,
					    cwd: "<%=paths.dev.templates%>",
					    src: ['**'],
					    dest: "<%=paths.dist.templates%>"
					}
				]
			}
		},
		sass: {
			dist: {
			    options: {
				style: 'expanded',
				compass: true
			    },
			    files: [
				{
				    expand: true,
				    cwd: "<%=paths.dev.styles%>",
				    src: ['*.scss'],
				    dest: "<%=paths.dist.css%>",
				    ext: '.css'
				}
			    ]
			}
		},
		clean: {
			styles: ["<%=paths.dist.css%>"],
			scripts: ["<%=paths.dist.js%>"],
			templates: ["<%=paths.dist.templates%>"]
		},
		express: {
			options: {
				background: true
			},
			dev: {
				options: {
					script: "<%=paths.express%>/app.js"
				}
			}
		},
		watch: {
			all: {
				files: ["<%=paths.dev.scripts%>/**/*.js", 
					"<%=paths.dev.styles%>/**/*.scss",
					"<%=paths.express%>/**/*.js",
					"<%=paths.dev.templates%>/**/*.html",
					
				],
				options: {
					livereload: true,
					spawn: false
				},	
				tasks: ['uglify', "sass", "express:dev"]
			},
		}
	});

	require('load-grunt-tasks')(grunt);
	
	grunt.registerTask('build', [
		'clean',
		'sass',
		'uglify',
		'copy'
	]);

	grunt.registerTask('serve', [
		'express:dev',
	]);

	grunt.registerTask('default', [
		'build',
		'serve',
		'watch'
	]);
}
