# Enigma Angular Ean Demo Test ====================

This is a simple AngularJS application and Express JS sever demo to perform basic GET and POST to save and retrieve data. This demo makes use of AngularJS, Javascript, jQuery, Twitter Bootstrap, NodeJS, and ExpressJS

# Pre-Requisites ==========================================

This project utilises Node, Bower, Grunt, Express, Etc. so These are required for this project to run. Client and server libraries are present in respective package.json and bower.json files.

To get up and running, just run:

      sudo npm install 

Depending on your permission settings, you may have to omit sudo from the
command. This would install all dependencies listed in package.json. Now the
following command is needed to be executed

      bower install

This will install all vendor dependencies in bower_components folder.

NOTE: It is crucial that in order to run the task runner, all bower components
should be installed in their default paths, as the task runner is configured to
use default paths.

# Development ==========================================

I wanted to create something quick, efficient, innovative and usable. I chose MEAN stack inspite of me being a complete Mongo Novice. I've decided not to use Mongo for now and would save requests data in memory.

The client is a angular js based 2 page application, which are loaded dynamically at run time. The main.html loads up a "doughnut/piechart" and addItem.html has a form where one can use to save data to back end.

# File Structure ========================================================


      Root
      |
      |`-----------------------------------------------
      |   |                                  |
      |    `-- bower_components/             `-- node_modules/
      |              (ommitted)                     (ommitted)
      |
      |`-----------------------------------------------
      | |                              |
      | `-- dev/                       `-- dist/
      |  |                               |
      |  |`-- img/                       |`-- img/
      |  |      (ommitted)               |     (ommitted) 
      |  |                               |
      |  |`-- fonts/                     |`-- fonts/
      |  |      (ommitted)               |     (ommitted) 
      |  |                               |
      |  |`-- scripts/                   |`-- js/
      |  |  |                            |  `-- enigma.js
      |  |  |`-- config/                 |
      |  |  |  `-- config_chartjs.js     |`-- sass/ 
      |  |  |  `-- config_enigma.js      |  `-- enigma.css 
      |  |  |  `-- config_routes.js      |
      |  |  | 
      |  |  |`-- controllers/
      |  |  |  `-- mainController.js
      |  |  |  `-- itemController.js
      |  |  |
      |  |  |`-- services/
      |  |  |  `-- xhrService.js
      |  |  |
      |  |   `-- application.js
      |  |
      |  `-- styles/
      |    `-- enignma.scss
      |    `-- _application.scss
      |
      |`-- server/
      |  |
      |  |`-- routes/
      |  |  `-- api.js
      |  |  `-- datam.js
      |  |
      |   `-- app.js
      |
      |`-- bower.json
      |`-- index.html
      |`-- GruntFile.js
      |`-- package.json
      `-- README.md

# BUILDING ============================================

Everything has been automated using the Grunt Taskrunner. Provided that
GruntFile.js, and dev/bower_compnents directories are intact, the script
can be built by running the command:

     grunt

This would clear out old files and directories, and create new resource
files (concatenated javascript), css, fonts and images. This command would 
end with a watcher watching the files directory.

The command also starts up the NodeJS webserver on port 3000 (as specified in configuration server/app.js) for the application to consume API services.

Minification has been turned off, as I assumed that you would like to have
a look at the generated script.


# END ==========================================

Although I tried to make this application look professional and bug free, there might
be few quirks or optimizations that I may have overlooked OR I didn't know of them.
Please feel free to contact me with further questions or Ideas at yprasannakumar@gmail.com

# NOTE ==================================================

If the APP seems broken, try accessing http://localhost:3000 on your browser, If you cannot access the service, then it means the service is down and so, the app is broken. You need to restart the service by running the grunt command.